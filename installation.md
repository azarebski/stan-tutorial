---
title: Stan workshop
author: Alexander Zarebski and Robert Moss
geometry: margin=3cm
urlcolor: blue
header-includes:
  - \usepackage{sectsty}
  - \allsectionsfont{\normalfont\sffamily\bfseries}
---

In this workshop you will learn to use the [stan](http://mc-stan.org/) programming language to solve real problems. Starting with a recap of basic Bayesian statistics we will move onto replicating an analysis from the world of conservation ecology. We will also cover diagnostics for the quality of samples in an MCMC simulation.

To ensure that we can dive straight in you will need to have a working installation or R and several packages. Installation instructions are included below.

If you run into trouble during the installation process please refer to the trouble shooting guide at the end of this document.

# **Required software - Windows**

## **R programming language**

* **You need R version 3.0.2 or later.** Preferably 3.2.0 or higher.
* Check if you have R installed on your machine by searching "All Programs" in the "Start" menu. If you already have R installed you may still **need to install** "Rtools" so please keep reading.
* A binary for R can be downloaded from The Comprehensive R Archive Network (CRAN) or through the link [here](https://cran.r-project.org/bin/windows/base/release.htm).
* Run the installer (the `.exe` file) you have just downloaded to get a windows-style installer.
* You will also need Rtools. If you have downloaded the most recent version of R you will need [Rtools34.exe](https://cran.r-project.org/bin/windows/Rtools/Rtools34.exe), otherwise grab the one that corresponds to your R installation. When installing Rtools, make sure that you check the box when prompted about the `PATH`. Detailed instructions are available [here](https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Windows#install-rtools)

## **RStudio**

RStudio is a free and open-source integrated development environment (IDE) for R. While not essential, RStudio can be very helpful when working in R. An RStudio executable for Windows can be downloaded from [here](https://download1.rstudio.org/RStudio-1.0.153.exe).

## **R packages**

One of the best parts of R is the community. Publishing an R package has become a common way to share new statistical methods; there's an R library for nearly every task you can image. You will need the following standard R packages

- [`dplyr`](https://cran.r-project.org/web/packages/dplyr/index.html)
- [`reshape2`](https://cran.r-project.org/web/packages/reshape2/index.html)
- [`ggplot2`](https://cran.r-project.org/web/packages/ggplot2/index.html)
- [`microbenchmark`](https://cran.r-project.org/web/packages/microbenchmark/index.html)

To install the `dplyr` package either start an R session by starting the R GUI or open RStudio and click on the "console" pane. If you are accessing R from the GUI you can then install `dplyr` from within the R session with

```R
install.packages("dplyr", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

If you are working in RStudio it should suffice to just use

```R
install.packages("dplyr")
```

The remaining packages are all installed in the same way.

## **RStan**

Install RStan by executing the following command in either the R GUI or the console of RStudio

```R
# note: omit the 's' in 'https' if you cannot handle https downloads
install.packages("rstan", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

**Restart** R and check that the installation has worked by executing the following code and checking the result is `10`.

```R
fx <- inline::cxxfunction( signature(x = "integer", y = "numeric" ) , '
	return ScalarReal( INTEGER(x)[0] * REAL(y)[0] ) ;
' )
fx( 2L, 5 ) # should be 10
```

# **Required software - Mac OSX**

## **R programming language**

* **You need R version 3.0.2 or later.** Preferably 3.2.0 or higher.
* Check if you have R installed on your machine by searching for "R" in your Applications. If you already have R installed you may still "Rtools" so please keep reading.
* A binary for R can be downloaded from The Comprehensive R Archive Network (CRAN) or through the link [`here`](https://cran.r-project.org/bin/macosx/R-3.3.3.pkg). This is not the latest version of R but shouldn't require you to install any additional tools. `R-3.4.1` is also available via CRAN but you'll need to make sure you've Clang 4.0.0 and GNU Fortran 6.1 installed.
* Run the installer (the `.pkg` file) you have just downloaded and it should walk you through the installation. The default options should all be fine.
* If you already have a C++ compiler and GNU-compatible `make` installed you can proceed. Otherwise, stan recommends installing the official Xcode from Apple available through the "App Store".

## **RStudio**

RStudio is a free and open-source integrated development environment (IDE) for R. While not essential, RStudio can be very helpful when working in R. An RStudio executable for Mac OSX can be downloaded from [here](https://download1.rstudio.org/RStudio-1.0.153.dmg).

## **R packages**

One of the best parts of R is the community. Publishing an R package has become a common way to share new statistical methods; there's an R library for nearly every task you can image. You will need the following standard R packages

- [`dplyr`](https://cran.r-project.org/web/packages/dplyr/index.html)
- [`reshape2`](https://cran.r-project.org/web/packages/reshape2/index.html)
- [`ggplot2`](https://cran.r-project.org/web/packages/ggplot2/index.html)
- [`microbenchmark`](https://cran.r-project.org/web/packages/microbenchmark/index.html)

To install the `dplyr` package either start an R session in your terminal (by running `R` at the command line) or open RStudio and click on the "console" pane. If you are accessing R from the command line you can then install `dplyr` from within the R session with

```R
install.packages("dplyr", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

If you are working in RStudio it should suffice to just use

```R
install.packages("dplyr")
```

The remaining packages are all installed in the same way.

## **RStan**

Install RStan by executing the following command in either the R GUI or the console of RStudio

```R
# note: omit the 's' in 'https' if you cannot handle https downloads
install.packages("rstan", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

**Restart** R and check that the installation has worked by executing the following code and checking the result is `10`.

```R
fx <- inline::cxxfunction( signature(x = "integer", y = "numeric" ) , '
	return ScalarReal( INTEGER(x)[0] * REAL(y)[0] ) ;
' )
fx( 2L, 5 ) # should be 10
```

# **Required software - Linux**

## **R programming language**

R can be downloaded for all major distributions [here](https://cran.r-project.org/bin/linux/).

## **RStudio**

RStudio is a free and open-source integrated development environment (IDE) for R. While not essential, RStudio can be very helpful when working in R. RStudio executables for Fedora and Ubuntu can be downloaded from [here](https://www.rstudio.com/products/rstudio/download/).

## **R packages**

One of the best parts of R is the community. Publishing an R package has become a common way to share new statistical methods; there's an R library for nearly every task you can image. You will need the following standard R packages

- [`dplyr`](https://cran.r-project.org/web/packages/dplyr/index.html)
- [`reshape2`](https://cran.r-project.org/web/packages/reshape2/index.html)
- [`ggplot2`](https://cran.r-project.org/web/packages/ggplot2/index.html)
- [`microbenchmark`](https://cran.r-project.org/web/packages/microbenchmark/index.html)

To install the `dplyr` package either start an R session in your terminal (by running `R` at the command line) or open RStudio and click on the "console" pane. If you are accessing R from the command line you can then install `dplyr` from within the R session with

```R
install.packages("dplyr", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

If you are working in RStudio it should suffice to just use

```R
install.packages("dplyr")
```

The remaining packages are all installed in the same way.

## **RStan**

Install RStan by executing the following command in either the R GUI or the console of RStudio

```R
# note: omit the 's' in 'https' if you cannot handle https downloads
install.packages("rstan", repos = "https://cloud.r-project.org/", dependencies=TRUE)
```

**Restart** R and check that the installation has worked by executing the following code and checking the result is `10`.

```R
fx <- inline::cxxfunction( signature(x = "integer", y = "numeric" ) , '
	return ScalarReal( INTEGER(x)[0] * REAL(y)[0] ) ;
' )
fx( 2L, 5 ) # should be 10
```

# **Trouble shooting**

Stan can be a bit finicky to install but the community has put together some
excellent and *very* detailed installation and trouble shooting documentation [here](https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started).
