---
title: README
author: Alexander Zarebski
geometry: margin=3cm
urlcolor: blue
---


### Getting these resources

The notes, souce code and some installation instructions for this tutorial are available online:

[https://bitbucket.org/azarebski/stan-tutorial](https://bitbucket.org/azarebski/stan-tutorial)

In fact, you may very well be reading this on the bitbucket page. Unfortunately, I haven't yet managed to get the mathematics to render on the bitbucket page. If you know how to do this feel free to put in a pull request.

## Introduction

This tutorial will introduce applied Bayesian statistics through the probabilitic programming language stan. No prior knowledge is assumed, although some experience with `R` would be useful. To be completely honest, I lean towards the Bayesian approach to statistics, so the following advice will be biased. I've tried to keep the tone impartial but with little success. I hope you find it of some use regardless.

### What is the Bayesian approach?

Assume we have some data, $D$, and we will assume it came from a model with parameters, $\theta$. Disregarding the huge literature exploring the foundations of statistics, let's just think of $\theta$ as a random variable and then we'll roughly end up with Bayesian statistics. Assume that we start with some prior beliefs about the value of $\theta$. This belief can be expressed as a distribution, $\pi(\theta)$, over the possible values of the parameter. The "problem" in Bayesian statistics is to determine how observing the data, $D$, has changed our beliefs about $\theta$. The "solution" for a will typically be the "posterior distribution", or the distribution of the parameters, $\theta$, conditional upon $D$, the observed data. The theorem that makes all this possible, named after Thomas Bayes is:

$$
P(\theta|D) = \frac{P(D|\theta)\pi(\theta)}{P(D)}.
$$

The idea is that the distribution, $P(\theta|D)$, now represents our belief after witnessing $D$.

### When and why you might choose a Bayesian approach over a frequentist one

There are foundational reasons to choose a Bayesian approach over a frequentist approach. I am of the opinion that the Bayesian approach is easier to reason *within*. Meaning it is easier to *think Bayesian* and this simpler mental model means we can spend more effort thinking about the problem at hand than about statistics in general. As with much of modelling, there are substantial gains to be had from being forced to make explicit our assumptions and reasoning. This is very apparent in the Bayesian culture where understanding the "generative model" is important. By having an explicit formulation of the *generative process* (through the model) and our beliefs (through the prior distribution) we get an explicit quantification of the uncertainty in our knowledge (the posterior distribution) which assists communication with stake holders.

So far, we've only considered the benefits in terms of "discovery", however there are also benefits for Bayesian methods in application of these discoveries. What on Earth do I mean by this? I mean that in addition to being useful for understanding data (*post*-diction) the ability to incorporate prior knowledge into inference assists in predicting future observations. The computational problems that held Bayesian methods back for so long are much less of a problem now, making this methodology available to all.

You may be starting to think that this is all starting to sound like propaganda, and in a sense it is, but I *did* warn you. The whole frequentist/Bayesian debate is kind of silly really, and as far as an applied researcher is concerned, not *really* relevant. In fact in many ways both approaches are effectively the same and it's only nit picking about the *meaning* of the results. What I hope to convey to you in this tutorial is that the Bayesian way is a viable choice and for some of you it may be a much more pleasant and productive way to work.

### Why stan is good if you don't like computers and statistics

People often call me out when I say "I don't like programming", but really what I don't like is typing. I *really* like to think about using computers to do stuff, but I find sitting down and actually making the computer do that stuff boring. This is why the stan programming language is perfect for me (and quite possible you too!)

![Andrew Gelman preaching](https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAj8AAAAJGU2MGFlYzNhLWU0ZTMtNGY2Ny04MGJmLTRlODg1OWZlMDM2Yg.jpg)

The folklore is that stan was conceived when Andrew Gelman got sick of implementing the same solver over and over again. So he (and some software developers) crafted a programming language which takes care of the boring parts meaning we only need to write a specification of our particular problem and stan will take care of all the dirty work of actually solving it. Another benefit of this situation is that once the problem is specified in stan you can choose one of several computational methods for the inference you perform. So even if a Bayesian approach isn't appropriate for a particular problem, you can still use stan to generate point estimates and confidence intervals.

## A simple example

![](https://upload.wikimedia.org/wikipedia/commons/d/d4/Thomas_Bayes.gif)

This *may* be a picture of Thomas Bayes, the English statistician, philosopher and minister. While not published until after his death (in 1761), Bayes' essay: *An Essay towards solving a Problem in the Doctrine of Chances* is the first work to express the idea of the posterior distribution of the parameters conditional upon the data.

### Abstract

> *Given* the number of times in which an unknown event has happened and failed: *Required* the chance that the probability of its happening in a single trial lies somewhere between any two degrees of probability that can be named.

### Problem specification for a contemporary audience: "Heads or tails?"

For ease of exposition let's just consider a special case of the problem addressed in the paper above. So many things in life are determined by the flip of a coin, but how can we actually know whether a coin is fair? In this section we will consider the problem of determining the probability that a coin is biased towards heads after observing a sequence of flips.

### Problem specification for a statistician: "Getting your priorities straight"

Suppose an experimentalist has flipped a coin a predetermined number of times, $n$, and in doing so recorded the number of heads. It's usually a good idea to start with a simple model so let's start by modelling the number of heads as a binomial random variable (assuming the coin can't land on its side).

$$
X \sim \text{Bin}(n, p).
$$

Since $n$ was a fixed part of the design there is only one parameter in the model, $p$. In a frequentist analysis this is where we would usually write down a likelihood function and maximise it to compute the maximum likelihood estimate. In our Bayesian analysis the next step is to think about any *prior knowledge*  we have about coin flipping. Now, if this was stock standard coin I might be tempted to say I have a pretty strong belief that it is about fair with maybe a slight bias, but only slight. But this could be any coin; maybe it has heads on both sides, or tails on both sides. There isn't enough known so I'm going to be very cautious. Let's say that under our prior knowledge, any value of $p$ is equally likely. Therefore our *prior distribution* is uniform on $[0,1]$.

$$
p \sim \text{U}(0, 1).
$$

Note that the uniform distribution over the interval $[0,1]$ is a special case of the [beta distribution](https://en.wikipedia.org/wiki/Beta_distribution). In fact it is a $\text{Beta}(1,1)$ distribution. The beta-distribution has the very nice property that it is the "conjugate" prior for the binomial distribution. This means that if we assume a beta prior distribution the posterior distribution (i.e., the distribution of our belief in the parameter after we have seen the data) is still beta.

### Pen and paper solution *or* "1763 called and they want their example back"

As I told you before, the beta distribution is conjugate to the binomial model. This means that if you flip $n$ coins and $X$ of them come up as heads the posterior distribution is

$$
\text{Beta}(1 + n - X, 1 + X)
$$

In which case questions such as "what is the probability the coin is biased towards heads" is simple! In an `R` session try the follow commands.

```R
n <- 10
X <- 7
pbeta(0.5, 1 + X, 1 + n - X, lower.tail = F)
# pbeta(0.5, 1 + n - X, 1 + X)
```

which tells us the probability that the coin is biased towards heads is $0.8867188$. Setting `lower.tail = F` ensures that we are calculating the probability of heads being *greater* than a half. However as has been pointed out to me since you could just reverse the arguments and avoid this issue. To see why this is true think about the functional form of the beta distribution.

### Problem specification for stan *or* "I don't just type, I type"

Solving this problem in stan equates to specifying a distribution to draw samples from and then using these samples to answer the question. Simple. In a new file called `coin.stan` we will make code this up in stan. For the purposes of explaining this I'll do things in a slightly different order to how they will appear in file at the end, but I think it is clearer to build a model this way. Keep in mind that stan is a different programming language to stan so you can't mix the two in one file. Subsequently I tend to implement my stan modes in one file and write another R script which uses it.

Now, without further ado, let's write down the "model" we hypothesise to be the generative process for the data

```stan
model {
  p ~ uniform(0, 1);
  x ~ binomial(n, p);
}
```

Note the keyword `model` at the start which tells stan the thinks between the following brackets specify the model which generated the data. It's really simple, we just tell stan that the parameter $p$ came from a uniform distribution and $X$ (conditional upon $p$) is binomial with a known number of samples $n$. Alternatively, since $\text{Beta}(1,1)$ is the same as a uniform distribution we could have written this as

```stan
model {
  p ~ beta(1, 1);
  x ~ binomial(n, p);
}
```

Clearly $n$ and $X$ are data that we need to give to stan. So the next step is to write the "data" block which is the way we send information into stan. The only difficulty with this is that we need to give stan a little bit of information about the data so it knows what to expect. In this case both the number of trials and the number of successes are integers so we preface their declaration with `int`. This tells stan that these variables have the *type* "integer".

```stan
data {
  int n;
  int x;
}
```

Finally, stan needs to know what parameter you are attempting to estimate. This is done through a "parameters" block.

```stan
parameters {
  real<lower=0,upper=1> p;
}
```

This is only a slight variant on the declarations in the data block. Since the parameter is a real number we use `real` as its type. There is some additional information here though. We have specified `lower` and `upper` bounds on the parameter (it is a probability after all). Strictly speaking this isn't essential, but stan is pretty clever and in some cases letting it know the range of possible values a variable can take can mean avoiding computational issues. It's just a good habit to get into, besides, along with specifying the types of the variables this is a nice way to force you to specify the model very explicitly.

The complete specification, which we'll save in a file called `coin.stan` should look like the following. 

```stan
// coin.stan

data {
  int n;
  int x;
}

parameters {
  real<lower=0,upper=1> p;
}

model {
  p ~ beta(1,1);
  x ~ binomial(n, p);
}
```

Two things to note here. The double backslash indicates a comment and the block appears in matters!

### Stan's solution

We can now run this model in R. You could enter the following command in an R session, but to make sure you can repeat the analysis you might want to follow my lead and put them in a `main.R` script.

```R
# main.R

library(rstan)

n <- 10
X <- 7
fit <- stan(file = "coin.stan", iter = 1E5)
p_samples <- as.data.frame(fit)$p
mean(p_samples > 0.5)
```

which gives us the solution $0.88802$ which is incredibly close to our pen and paper solution.

#### Frequentist solution

If you only want a point estimate (and approximate confidence intervals) you don't need to do a full sampling run. Stan provides some optimisation routines which allow you to calculate the maxima of the target log-likelihood function. If you include a prior you can think of this as maximum a posterior probability estimate, of a regularised least squares regression. If you remove the prior distribution this is very similar to the stock standard MLE approach.

```R
library(rstan)

n <- 10
X <- 7
coin <- stan_model("coin.stan")
optimizing(coin)
```

which gives us the point estimate $\hat{p}=0.6999909$. In this case, since we have used a uniform prior distribution both the MLE and the maximum posterior estimate are the same.

### Exercise

1. Run all the code above and compute the posterior mean of the parameter $p$, i.e., the mean value of the parameter under the posterior distribution. Hint: `p_samples`
2. How would the prior distribution need to change if we knew for a fact, before observing the data, that the coin was capable of coming up heads (i.e., it is not the case that the coin has tails on both sides)?
3. Adjust the stan model so that it has the modified prior distribution which removes the possibility of the coin not being able to come up either heads or tail. Hint: `uniform(0, 1)` is the same as `beta(1, 1)`

## Working in stan

Solving a real problem

### Harbor seals in Washington State

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Common_Seal_Phoca_vitulina.jpg/1920px-Common_Seal_Phoca_vitulina.jpg)

After a bounty program to reduce habor seals (a.k.a. "harbour seals", or "common seals") in Washington State ended in the 60's a program initiated to return the population to its natural state. One of the difficulties for this program was monitoring the seal populations to understand how the population was progressing. Through the 70--90's ariel surveys were conducted on seal populations in multiple locations around Washington state. In this section we will study one of the time series collected (from a coastal estuary) and perform our own analysis of their data to answer a real problem in conservation ecology.

> Is the seal population greater than half its carrying capacity?

The relevance of this question is that management of the seal populations was to change hands once this was reached. They defined it in a slightly different but -- for our model at least -- equivalent way.

### Growth model

We will model the *average population size* with a logistic growth curve, $s(t)$, which is defined by the following differential equation.

$$
\frac{ds}{dt} = r s \left( 1 - \frac{s}{K}\right).
$$

The time series of seal counts is in years starting from 1975, so the natural unit of time is years and we will treat $t$ as being time in years since 1975. The parameters of this model we need to estimate are:

* the initial condition: $s(0) = s_{0}$
* the growth rate: $r$
* the carrying capacity: $K$

There are, of course a lot of extensions to this model that could be used but this seems to be one of the simplest that has a size dependent growth rate which saturates. If more flexibility was desired one could opt for [generalised logistic growth](https://en.wikipedia.org/wiki/Generalised_logistic_function).

#### Observation model

The growth model provides us with a curve describing the seal population as a real number for a given set of parameters. i.e., given a set of parameters $(s_{0}, r, k)$ we get $s(t)$ at each of the observation times $t$. But how does this connect to the count data that has been obtained by the surveys? Well a simple model to start with will be to say that the observation at year $t$ -- which we will denote: $Y_{t}$ -- is a Poisson random variable with expected value: $s(t)$.

$$
Y_{t} \sim \text{Poisson}(s(t))
$$

and we will assume that given the $s(t)$ the observations are independent.

#### Prior knowledge

Now, I don't really know much about ecology or biology, but let's pretend I spoke to a seal expert before looking at the data and they told me that they thought that at the end of the bounty program there were about a thousand harbor seals in the coastal region, give or take several hundred. I then need to translate this "prior knowledge" into a prior distribution for my initial condition, $s_{0}$. To be safe let's say that our prior belief about the initial condition, $s_{0}$, is

$$
s_{0} \sim N(1000, 200).
$$

Where we truncate this distribution to be non-negative. In practise those this isn't really necessary as the probability of $s_{0}<0$ under this distribution is very small. As for the total population size, I'm guessing that is going to be in the thousands with a pretty decent tail so let's use a log-normal prior for $K$:

$$
K \sim\text{Lognormal}(\log(1000), 1)
$$

which looks like

![Log-normal prior distribution used for the carrying capacity, $K$.](jeffries-2003/lognormal_prior.png)

Now, my ecologist/biologist couldn't really quantify their prior belief about the growth rate of seals, but let's to a back of the envelope type calculation to see what we can deduce. First, I know that the seals should be increasing. So I'm happy to say $r$ is going to be positive. This means I want a distribution which says $r$ is a *positive* number (i.e., I won't be using an exponential distribution).

I know that initially the seal growth will look exponential (think about how newly introduced species will often have an explosive spread). So let's pretend for a moment that

$$
s(t) = s_{0} e^{r t}.
$$

I know for animals like seals it takes several years to reach sexual maturity and they are kind of biggish animals so I suspect, as an upper bound, the population shouldn't double in less than 2 years. Which roughly means

$$
2 > e ^{2r} \quad\implies\quad r < \log(2) / 2 \approx 0.35
$$

This is only a rough guess though and really I think $r$ could take nearly any positive value. So I'm going to say

$$
r \sim \text{Rayleigh}(0.1)
$$

which looks like

![Rayleigh prior distribution used for the growth rate, $r$.](jeffries-2003/rayleigh_prior.png)

This method of selecting a prior distribution which will generate plausible data is not "cheating" in the Bayesian sense. I haven't used the data I will be analysing.

### The data

The data is a time series of observed population counts in different years. As with lots of real data, there are missing counts from when they were unable to make the observations (due to funding or weather restrictions). The seal data is stored in the file `jeffries-2003/seals.csv`. Running `head seals.csv` shows us that the data looks like

```CSV
year,average_annual_count
1975,1694
1976,1742
1977,2082
1978,2570
1979,NA
1980,2864
1981,4408
```

Plotting the whole time series we get the following

![Time series of the number of seals observed each year.](jeffries-2003/seals.png)

### The data (as seen by stan)

As in the example above we need to declare the data in the `data` block. This code can all be found in the file `logistic.stan`.

```stan
data {
  int num_times;
  real times[num_times];
  int num_obs;
  int Y[num_obs];
  int obs_times[num_obs];
}

transformed data {
  real x_r[0];
  int x_i[0];
}
```

The variable `num_times` describes the number of times that an observation could have been made in the time series while `num_obs` describes the number of actual observations. The reason for having both of these is that there are missing data so `num_times > num_obs`. Then there are the actual times `times` and the times at which an observation was made `obs_times`. The actual observations being stored in the array `Y`. Note that the length of this array is `num_obs`, *not* `num_times`.

The `transformed data` block is where you should do any necessary pre-processing. I find that it is *usually* easier to do any such pre-processing in R and then read the processed values into stan. The empty arrays `x_r` and `x_i` are not really relevant to us but are needed later. Feel free to ignore them for now.

### Basic building blocks of a stan model *or* "Modelling as software development"

So we've got a model specified, which consists of the prior distribution of the parameters and a process which goes from parameters to the data. The next step is to translate that specification into something stan can make sense of. As before we will start by describing the "model" component.

```stan  
model {
  s_0 ~ normal(1000, 200);
  K ~ lognormal(log(1000), 1);
  r ~ rayleigh(0.1);
  for (t in 1:num_obs) {
    Y[t] ~ poisson(s[t]);
  }
}
```

The prior distribution for $s_{0}$, $K$ and $r$ is very similar to the one we constructed previously but the likelihood information is different now because we have a time series of observations rather than a single summary statistic (e.g., the number of heads in the previous example). As a first pass, let's just loop over the observations (which are stored in the array `Y`) and treat each as coming from a Poisson distribution.

I know the parameters I want to infer, so at this point I can tell stan what these are.

```stan
parameters {
  real<lower=0> s_0;
  real<lower=0> K;
  real<lower=0> r;
}
```

So far this seems to be pretty much exactly the same as what we did in the previous example but things are about to get very exciting. We are going to introduce a new "block", the `transformed parameters` block. The following is an important idea so I'm going to put in a quote environment.

> The transformed parameters block should contain the definitions of all variables which are deterministic function, of the parameters and the input data.

Making this idea explicit may seem a little pedantic, but it can be very useful when trying to reason about the final program so it's worth pausing for a moment to internalise this idea. Okay preaching over! Let's actually write the thing. What is the function of the parameters that we need? Well looking back to the "model" block above there is a variable `s` which is an array of the mean value of the Poisson distribution the $Y_{t}$ are assumed to be drawn from. So the random variables in our mathematical model, $Y_{t}$, are represented by the elements of an array, `Y`, in our code. The logistic curve, $s_{t}$, in our model is represented by the array, `s` in our code. The `transformed parameters` block makes this explicit:

```stan
transformed parameters {
  real s[num_obs];
  s = logistic_curve(num_times, times, num_obs, obs_times, s_0, r, K, x_r, x_i);
}
```

There are two new pieces of syntax here. The first is in the declaration of the variable `s`. It's made up of real numbers, but the square brackets indicate that it's an array of real numbers. Moreover, the `[num_obs]` tells stan that it should be an array of length `num_obs` many real numbers.

The second new piece of syntax is the use of a *user defined function*. The creatively named `logistic_curve`. Let's pause and think about this call to `logistic_curve`. It returns an array of the expected population size at each of the observation times (stored in `obs_times`). Recall that in the data we have there are missing data so we need to distinguish between the years of the study (stored in `times`) and the years in which an observation was actually collected, `obs_times`. The parameters of the growth curve: $s_{0}$, $r$, and $K$ of course also need to be passed as arguments. Finally there are these weird `x_r` and `x_r`. These play a role in solving differential equations and have to be included. Basically, they can be used to pass variables around in a slick way but for now let's just ignore them.

Using `logistic_curve` we have just moved the hard work elsewhere. On the plus side though we are about to be introduced to a new programming block. This block contains all the *function* definitions so it is called the "functions" block. Wow. A lot of the hard work ends up getting moved into the "functions" block and they can be a bit daunting at first so let's just work though this step by step. We start with the declaration of the function we are about to define.

```stan
functions {
  real[] logistic_curve(int num_ts, real[] ts, int num_obs, int[] obs_ts,
                        real s_0, real r, real K, real[] x_r, int[] x_i) {
    ...
  }
}
```

Where the `...` is where we define the actual computation. The declaration starts with `real[]` which means the function is going to return an array of real numbers of indeterminate length. There is also the declarations of the types of all the arguments. Obviously when I was first wrote this code I didn't know all of it in advance, and it generally takes me a couple of iterations to get function definitions correct but it's not as bad as it may look at first.. Luckily the stan compiler is quite friendly and helps to point out any type errors in the code. For example if you got one of the types wrong it would point this out to you and suggest the correct one too.

To be honest, I've been a little lazy here. I could have used an analytic solution to the logistic equation, and it would have left me with a *much* more efficient piece of code. But I wanted to demonstrate two things: first and foremost is how to use the ODE solvers in stan -- yes, there is an RK45 method and a stiff solver -- but a close second is that the numeric solver can often end up being clearer than a complicated analytic solution so it is sometimes easier to try brute first and then attempting to improve performance with an analytic solution later if need be.

Without further ado, here is the function definitions

```stan
functions {
  real[] logistic_curve(int num_ts, real[] ts, int num_obs, int[] obs_ts,
                        real s_0, real r, real K, real[] x_r, int[] x_i) {
    real sol[num_ts,1];
    real s[num_obs];
    real y0[1];
    real theta[2];
    y0[1] = s_0;
    theta[1] = r;
    theta[2] = K;
    sol = integrate_ode_rk45(logistic_diff, y0, 0, ts, theta, x_r, x_i);
    for (obs in 1:num_obs) {
      s[obs] = sol[obs_ts[obs],1];
    }
    return s;
  }
}
```

Note the use of `integrate_ode_rk45` which is one of the built-in solvers. Moreover, use of both of the ODE solvers is the same so if needed you could swap that out with `integrate_ode_bdf` which is the built-in stiff solver. Unless you have reason to believe you need it though, RK45 is faster. But wait, how does the solver know the differential equation we are solving? That information comes from the function `logistic_diff` I've snuck in there. This function takes the parameters, the current state of the system, $s(t)$, and time of the system and returns the differential we are integrating.

```stan
real[] logistic_diff(real time, real[] s, real[] theta, real[] x_r, int[]x_i) {
  real dsdt[1];
  dsdt[1] = theta[1] * s[1] * (1 - (s[1] / theta[2]));
  return dsdt;
}
```

Therefore the total block is

```stan
functions {
  real[] logistic_diff(real time, real[] s, real[] theta, real[] x_r, int[]x_i) {
    real dsdt[1];
    dsdt[1] = theta[1] * s[1] * (1 - (s[1] / theta[2]));
    return dsdt;
  }

  real[] logistic_curve(int num_ts, real[] ts, int num_obs, int[] obs_ts,
                        real s_0, real r, real K, real[] x_r, int[] x_i) {
    real sol[num_ts,1];
    real s[num_obs];
    real y0[1];
    real theta[2];
    y0[1] = s_0;
    theta[1] = r;
    theta[2] = K;
    sol = integrate_ode_rk45(logistic_diff, y0, 0, ts, theta, x_r, x_i);
    for (obs in 1:num_obs) {
      s[obs] = sol[obs_ts[obs],1];
    }
    return s;
  }
}
```

Stan doesn't yet have an easy way to import functions between model files but hopefully it will soon.

### Running the sampler

Okay, so now we have our stan model defined in one file. The next step is to actually use it to analyse the data. In a contemporary Bayesian analysis this usually means drawing samples from the posterior distribution and using these samples to answer questions. The following R snippet does just that. It reads in the seal data (from `seals.csv`) into a dataframe, `df`, and proceeds to define the objects used by the stan model. We need to have objects defined for each of the things declared in the `data` block which is shown as a comment in the R snippet. Finally it runs the sampler to obtain the posterior samples and prints a summary. The code to actually run this analysis is in `main.R`.

```R
library(rstan)

df <- read.csv("seals.csv")[1:10,]
df$times <- df$year - min(df$year) + 1
obs_mask <- !is.na(df$average_annual_count)
obs_df <- df[obs_mask, ]

# data {
#   int num_times;
#   real times[num_times];
#   int num_obs;
#   int Y[num_obs];
#   int obs_times[num_obs];
# }

num_times <- nrow(df)
times <- df$times
num_obs <- nrow(obs_df)
Y <- obs_df$average_annual_count
obs_times <- obs_df$times

post_samples <- stan("logistic.stan")
print(post_samples)
```

The final line compiles the stan source code and runs the sampler with the default settings (4 chains with 1000 samples of warmup and 1000 kept as posterior samples). If you want to find out more about the `stan` function, or any R function for that matter, just type `?stan` (or `?<function>` into an interactive session). Running `post_samples <- stan("logistic.stan")` takes a while because it has to compile the model. The compiled model is stored though so in subsequent calls it only needs to do the actual sampling. In this case the compilation will be the most time consuming part, but for more in depth analyses the sampling is the expensive part.

One of the scariest things that can go wrong when running the sampler is that there will be "divergent" samples. Don't worry, stan will warn you if this happens. These tarnish the quality of all future samples but stan is good about suggesting potential ways to fix this. Details of checking the quality of the output from the call to `stan` and methods to improve the quality are given in the next section.

### Diagnostics and model fit

There are at least three main ways in which our solution can be wrong. While this may feel a bit like a "modelling 101" rant I want to spell these out in some detail to make what comes next easier:

0. ~~There could be a bug in my specification of the model~~
1. There could be numerical errors due to the problem being "difficult" for the algorithms to solve
2. The theoretical model I have constructed could not be very good for explaining what we are seeing

There isn't really a lot to say about the zero-th point that is stan-specific or even mathematical modelling specific. Just the observation that when debugging, the compiler will be your best friend when it catches your typos and your worst enemy while it makes you wait endlessly to see if your code compiles. So, let's just cross that one out and assume that our code is correct.

There is an important distinction in the first two points but one that is easy to overlook. I've put point one first because in a sense it is easier to handle. There are a bunch of stock standard tests you can perform and if these all come out fine, you won't necessarily know the sampler worked well, but you can be reasonably confident that it hasn't completely fallen over. We will go through these tests in the next section.

Now assuming the code is bug free, and the sampler can handle it without problems we still have the terrifying problem of whether our model is even sensible! To do this we will **plot everything**. But before this let's look at the sampler diagnostics.

#### Sampler diagnostics

The following figures make use of a couple of convenience functions provided by `rstan`.

```R
ggsave("diagnostic_trace.png", stan_trace(post_samples, "lp__"))
ggsave("diagnostic_rhat.png", stan_rhat(post_samples))
ggsave("diagnostic_ess.png", stan_ess(post_samples))
```

The first, and most basic is to look at a time series of the log-likelihood of each of the samples. We are looking here for a "furry caterpillar" with no apparent trend in the values. Hopefully all the chains will be overlapping so we can be confident that from multiple starting conditions the chains have all come to similar log-likelihoods, to ensure none got caught in a local minima.

![Diagnostic trace plot of the log-likelihood for each sample for each of the chains. Note the desirable "fuzzy caterpillar" and overlap between the chains which suggests they have all converged to their shared limiting distribution.](jeffries-2003/diagnostic_trace.png)

Now, we have seen the desired behaviour in these plots, but if there *was* a clear trend to the log-likelihood values, this would suggest that the chains haven't converged yet. i.e., the samples aren't really being drawn from the posterior distribution yet, recall this is only an asymptotic property of the samples. If this is the case you should increase the number of "burn-in" samples so these transient samples aren't included in the posterior samples.

If the trace plot has regions of constant value, this indicates that the chain is getting "stuck". The easiest solution to this is to just increase the "thinning" by some factor and increase the number of posterior samples by the same factor. Changing the number of burn-in samples and the thinning is done via the `warmup` and `thin` arguments to `stan()`.

The trace plot is perhaps better summarised by the $\hat{R}$ statistic. This statistic considers the ratio of between chain variance to within chain variance. i.e., if this statistic is much greater than $1$ then it means there is more variance between the chains than within them, suggesting they have not converged to their common limiting distribution yet. Although the trace plot can provide guidance in what to tweak to improve performance it can be hard to judge the quality from this alone. Hence the rule of thumb is that we want $\hat{R} < 1.1$.

![Diagnostic plot for the $\hat{R}$ statistic. As in this figure the majority of the histogram should be $<1.1$. If this is not the case you should try increasing the duration of the warm-up samples.](jeffries-2003/diagnostic_rhat.png)

Further information about whether thinning needs to be increased is provided by the "effective sample size" plot.

![Diagnostic plot for the effective sample size. The majority of the histogram should be nearish to $1$. If this is not the case you should try increasing the thinning of the samples.](jeffries-2003/diagnostic_ess.png)

Basically, you want these values to be near 1 (which is what they would be if you had truly independent samples). To increase their values you can just increase the thinning on the chain. But of course, doing so will mean you have to wait longer to get the same number of samples. So really it's up to you to determine how long you are willing to let the chain run.

#### Model diagnostics

Now that we have the sampler results in `post_samples` and have checked that the sampling has not obviously broken down we should investigate what the model fit tells us. To do this we need the solution at every time point, our current implementation only obtains the solution at the observation times. I can see a couple of ways to improve upon our current implementation. Maybe you would like to try your hand at implementing one?

Regardless, let's just push forward with a brute force solution. This is a little bit silly, but provides a good introduction to the "generated quantities" block. This block is for all the variables we are interested in but that are not directly part of the likelihood function. The purpose of this block is to generate predictions under the posterior distribution. For instance to impute the solution at all time points (i.e., including the ones where the data are missing) we could use the following

```stan  
generated quantities {
  real seal_pop[num_times];
  {
    int times_int[num_times];
    for (ix in 1:num_times) {
      times_int[ix] = ix;
    }
    seal_pop = logistic_curve(num_times, times, num_times, times_int, s_0, r, K, x_r, x_i);
  }
}
```

Running the same R snippet as before with this block included in the stan file obtains the solution for each posterior sample at all the time points in `times`. The following code then takes these values and visualises the results.

```R
library(reshape2)
library(ggplot2)

post_samples <- stan("logistic.stan")

post_df <- as.data.frame(post_samples)
seals <- select(post_df, starts_with("seal_pop"))
names(seals) <- as.character(1:25)
seals <- melt(seals, id.vars = c()) %>%
  group_by(variable) %>%
  summarise(m = mean(value),
    ymax = quantile(value, 0.975),
    ymin = quantile(value, 0.025),
    obs_max = mean(qpois(0.975, lambda = value)),
    obs_min = mean(qpois(0.025, lambda = value))) %>%
  mutate(time = as.integer(as.character(variable)))

g_fit <- ggplot() +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = obs_min, ymax = obs_max), fill = "green", alpha = 0.4) +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "blue", alpha = 0.4) +
  geom_line(data = seals, mapping = aes(x = time, y = m), colour = "blue") +
  geom_point(data = data.frame(x = obs_times, y = Y), mapping = aes(x = x, y = y), colour = "red") +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line()) +
  labs(x = "Year", y = "Observed population")
ggsave("model_fit.png", g_fit)
```

Actually the code above does a little more than just visualise the values of $s$ and the observed seal counts as we can see in the following figure

![Model fit assuming a Poisson observation model. The blue ribbon shows the $95\%$ credible interval for the mean population size and the green ribbon the $95\%$ credible interval for the observations. The Red points are the observed seal population sizes.](jeffries-2003/model_fit.png)

The blue ribbon contains the $95\%$ *credible interval* (CI) for the size of the seal population, with the median value as a solid line down the middle. From this we can see that the model is doing a good job of capturing the average behaviour of the seal population. But a bit of thought suggests we are not really comparing apples with apples here. Because the count data is *not* the average, it is the observations from a distribution which has mean value equal to the blue line.

Under the current model the count data is modelled as coming from a Poisson distribution with mean $s(t)$. This makes it simple to compute the $95\%$ CI for the *observations*. This *"credible interval"* is shown in the green ribbon. Since the observations are assumed to be independent observations we would assume that $95\%$ of the observations should fall in the green band. Clearly this is not the case so we observe the data are over-dispersed with respect to a Poisson model.

> practical Bayesian data analysis is an iterative process of model building, inference, model checking and evaluation, and model expansion. ~ Gabry *et al* (2017)

Okay, so we know we need to try an over-dispersed model for our count data. The two flavours of distributions which come to mind are: something binomial-esque, or something zero-inflated. There doesn't appear to be any zero counts in the data so I don't really think a zero inflated model would be appropriate. That leaves something in the binomial family, and since we have unbounded count data that pretty much means we should go negative binomial. Swapping out the observation model is simple, just replace `poisson` with `neg_binomial_2` which is a negative binomial parameterised by its mean and a dispersion parameter. This additional parameter needs to be declared in the `parameters` block leaving us with the following parameter and model blocks.

```stan
parameters {
  real<lower=0> s_0;
  real<lower=0> K;
  real<lower=0> r;
  real<lower=0> phi;
}

model {
  s_0 ~ normal(1000, 200);
  K ~ lognormal(log(1000), 1);
  r ~ rayleigh(0.1);
  phi ~ rayleigh(10);
  for (t in 1:num_obs) {
    Y[t] ~ neg_binomial_2(s[t], phi);
  }
}
```

The same R snippet as before will run the model with the adjusted observation model. However getting the credible interval is now slightly more involved so we can't just use the same plotting snippet. In this case the calculation of the green band is more involved because it is now a function both of the mean, $s(t)$, and the value of the dispersion parameter, $\phi$. This could be done as part of the stan code or, as done here, this could be done as part of the post-processing of the samples

```R
post_samples <- stan("logisticNB.stan")

post_df <- as.data.frame(post_samples)

seals <- select(post_df, starts_with("seal_pop"))
names(seals) <- as.character(1:25)
seals <- melt(seals, id.vars = c()) %>%
  group_by(variable) %>%
  summarise(m = mean(value),
    ymax = quantile(value, 0.975),
    ymin = quantile(value, 0.025)) %>%
  mutate(time = as.integer(as.character(variable)))

temp <- select(post_df, starts_with("seal"), phi)
names(temp) <- c(as.character(1:25), "phi")
seals_green <- melt(temp, id.vars = c("phi")) %>%
  group_by(variable) %>%
  summarise(ymax = mean(qnbinom(0.975, mu = value, size = phi)),
    ymin = mean(qnbinom(0.025, mu = value, size = phi))) %>%
  mutate(time = as.integer(as.character(variable)))

g_fit <- ggplot() +
  geom_ribbon(data = seals_green, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "green", alpha = 0.4) +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "blue", alpha = 0.4) +
  geom_line(data = seals, mapping = aes(x = time, y = m), colour = "blue") +
  geom_point(data = data.frame(x = obs_times, y = Y), mapping = aes(x = x, y = y), colour = "red") +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line()) +
  labs(x = "Year", y = "Observed population")
ggsave("model_fit_nb.png", g_fit)
```

The new fit is clearly worth the effort though as it provides a much more believable level of uncertainty around the population dynamics.

![Model fit assuming a negative binomial observation model. The blue ribbon shows the $95\%$ credible interval for the mean population size and the green ribbon the $95\%$ credible interval for the observations. The Red points are the observed seal population sizes.](jeffries-2003/model_fit_nb.png)

### Exercise

1. Make sure that you can run stan samplers with both of the observation models
2. How would you go about comparing these models to determine the one with the better fit in a systematic way? I'll give you hint. It rhymes with "Ray's tractor".
3. Which model does provide a better fit?
4. How would you compute the observation credible interval from within stan?

### Answering the question

The initial question we posed was:

> Is the seal population greater than half its carrying capacity?

To be honest I don't know why this was set as the target population size by the conservationists, however I suspect it has something to do with the dynamics of the growth model. The seal population's rate of increase is largest at $K/2$, so in a sense, setting the target higher than this has diminishing returns.

#### Formulation

For now, let's just focus on answering the question though. Mathematically, we are interested in calculating the following value:

$$
P(s(t_{\text{current}}) > K/2 | s(t), t = 1,\dots,t_{\text{current}}).
$$

The expression $s(t_{\text{current}}) > K/2$ is a deterministic function of the parameters. Let $\theta=(r,K,s_{0})$ be the parameters of the model and $f(\theta)$ be a function which is $1$ if $s(t_{\text{current}}) > K/2$ and $0$ otherwise. Another way to say this is that $f$ is the indicator function of the event that $s(t_{\text{current}}) > K/2$. With these definitions we can express the probability above as and integral over the posterior distribution:

$$
\int_{\Theta} f(\theta) p(\theta | \text{Data}) d\theta
$$

where $\Theta$ is the parameter space and $p(\theta | \text{Data})$ is the posterior distribution of the parameters. This integral, and hence the probability we were initially interested in, can be approximated by the sample mean of $f(theta)|\text{Data}$. To obtain this we draw $N$ samples (or rather stan does this for us) and see for what proportion of these the seal population is above $K/2$.

#### Implementation

Now that we have a firm grasp on the expression we want to evaluate and the computational method for doing so, we have to implement this. This involves computing the value of $f$ for each posterior sample in the generated quantities block

```stan
generated quantities {
  int f;
  if (s[num_obs] > (0.5 * K)) {
    f = 1;
  } else {
    f = 0;
  }
}
```

Even a cursory examination of the data suggests $s > K/2$, so it is not surprising that for all of the posterior samples this statement is true. The answer to the conservationists is that the seal population is greater than half it's carrying capacity. This statement comes with the caveat that the probability the population is not above this value is less than the Monte Carlo error from approximating the integral with posterior samples

We can do a little bit better than this though. By plotting the values of $K$ in the posterior samples we can obtain an approximation to the posterior distribution of the carrying capacity. There are two things we should be taking away from this the histogram of the posterior samples of $K$. The first is that this is very different to the prior distribution, indicating that this is actually information contained in the data. The second is that there is reasonable convergence to a value of $\approx7500$ indicating that the seal population as it stands at the end of the time series is roughly at it's carrying capacity.

![Posterior distribution of the carrying capacity under the negative binomial observation model.](jeffries-2003/carrying_capacity_nb.png)

### Exercise

1. Try re-doing this analysis with fewer data points so it is less clear that the seal population is above half its carrying capacity.
2. Since there is an [analytic solution](https://en.wikipedia.org/wiki/Logistic_function) available we didn't really need to use the ODE solver. Remove the reliance on the numeric solver in the code. Time both implementations.

## Advanced topics

### Vectorisation

Vectorisation (a.k.a., "array programming") refers to writing code such that operations are applied to arrays rather than their scalar constituents. For example, using matrix multiplication instead of looping over the elements of the matrix. There are two reasons you should care about this. The first, is that vectorised code is usually easier to understand as you spend more time thinking about the problem than how the computer is actually going to solve it. The second is that often it often is substantially faster to evaluate vectorised code. Stan is no exception. This is because when stan code is compiled to C$++$ automatic differentiation is used and subsequently more expressions in the stan source leads to less efficient code.

#### Example

Instead of taking my word for it, let's work through a simple example to see the benefits of vectorisation. While this is only a toy example, good vectorisation can be very useful when you are working on problems that are very computationally intensive. Consider the following simple model

$$
Y \sim N(\beta_{0} + \beta_{1}X_{1} + \beta_{2}X_{2}, \sigma^{2}).
$$

Let's assume we know $\sigma= 1$ and that the prior distribution is $\beta_{i}\sim N(0, 2)$ for all the $i$. The following code shows a simple way to implement this in stan.

```stan
data {
  int num_obs;
  matrix[num_obs,3] X;
  real Y[num_obs];
}

parameters {
  vector[3] beta;
}

model {
  beta[1] ~ normal(0, 2);
  beta[2] ~ normal(0, 2);
  beta[3] ~ normal(0, 2);
  for (i in 1:num_obs) {
    Y[i] ~ normal(X[i, 1] * beta[1] + X[i, 2] * beta[2] + X[i, 3] * beta[3], 1);
  }
}
```

This code is easy to understand, is correct, and if you just want something that runs so you can clock off for the day it will tick that box. Let's save it in the file `loopy.stan`. At this point that you may remember the numerical analysis lectures delivered by a terrifying A/Prof who would be most displeased with this code. So you open you're text editor again and vectorise the model block

```stan
model {
  beta ~ normal(0, 2);
  Y ~ normal(X * beta, 1);
}
```

Much nicer! That is much clearer. Even if it isn't any faster, it just *looks* better and so will be easier to come back to in a week's time when you've forgotten all about the implementation. Let's save it in `vec.stan` and see how the implementations stack up. The following R script will simulate $100$ data points and then fit each implementation $100$ times and report back on the number of seconds this took.

```R
library(rstan)
library(ggplot2)
library(microbenchmark)


set.seed(2)
num_obs <- 100
b <- c(1, -2, 3)
X <- matrix(rnorm(num_obs * 3), ncol = 3)
Y <- as.vector(X %*% b)

fit_dummy <- stan("vec.stan", data = c("num_obs", "X", "Y"))
vec_bm <- microbenchmark(stan("vec.stan", data = c("num_obs", "X", "Y")))
fit_dummy <- stan("loopy.stan", data = c("num_obs", "X", "Y"))
lpy_bm <- microbenchmark(stan("loopy.stan", data = c("num_obs", "X", "Y")))
plot_df <- rbind(as.data.frame(vec_bm), as.data.frame(lpy_bm))
g <- ggplot(data = plot_df, mapping = aes(x = expr, y = time / 1E9)) +
  geom_boxplot() +
  labs(x = "Expression", y = "Seconds") +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line())

ggsave("performance.png", g)
```

It takes a little while but the results are pretty clear

![Box plots of the time required to run the sampler with vectorised, and looping code. Substantial speed improvements can be obtained by vectorising code.](vec-sim/performance.png)

The vectorised version comes in at roughly twice as fast. This isn't exactly game changing, but this can still double the rate at which you can iterate on your model.

#### Exercise

1. Look over some of the other model implementations we have seen. Can you vectorise any of the code in them?
2. In this example we hard coded the three predictors in the model. Can you fix the code so that it can use an arbitrary number of predictors?

### Re-parametrisation

There is one warning that stan can return that chills me to the bone; a warning that you may need to re-parametrise the model. To understand this you first need to understand that stan uses a fixed mass matrix, this approach is called Euclidean Hamiltonian Monte Carlo (there is an alternative, called Riemannian Hamiltonian Monte Carlo, but there aren't computationally efficient ways to use it yet). In the Euclidean method, the warmup samples are used by stan to get an estimate for the correlations between parameters. These estimated correlations are then fixed for the remainder of the sampling. Therefore, if there are substantial non-linear dependencies between the parameters the sampler can have a very difficult time generating posterior samples because it has estimated a fixed relationship early on. This goes a bit beyond what you actually need to know to use HMC, but basically, the distribution is too curvy and stan freaks out.

The solution is to transform the model so the distribution is smoother. The following example, due to Radford Neal, is the stock standard example but there are additional examples in the stan documentation.

#### Neal's funnel

In this example we are just going to draw samples from a distribution, no data involved, but this isn't just any distribution. This is Neal's funnel, a pathological example of a distribution which none the less has properties shared by some models that appear in the literature. Consider the following random variables:

$$
Y \sim N(0, 3) \\
X_{i} \sim N(0, e^{Y/2})\quad\text{for}\quad i = 1,\dots,9
$$

We can draw samples of $Y,X_{1},\dots,X_{9}$ using the following stan model

```stan
parameters {
  real y;
  vector[9] x;
}

model {
  y ~ normal(0, 3);
  x ~ normal(0, exp(y / 2));
}
```

But doing so you'll be bombarded with warnings from stan due to the strange geometry of the distribution being sampled. Another specification of the same model can be constructed which is much easier for stan to sample from because it is essentially just sampling independent multivariate normals

```stan
parameters {
  real y_raw;
  vector[9] x_raw;
}

transformed parameters {
  real y;
  vector[9] x;
  y = 3.0 * y_raw;
  x = exp(y / 2) * x_raw;
}

model {
  y_raw ~ normal(0, 1);
  x_raw ~ normal(0, 1);
}
```

A comparison of the posterior samples suggests that the second implementation has done a far better job of exploring the posterior distribution

![Posterior samples from Neal's funnel under each of the parameterisations.](funnel/neal_funnel.png)

You an see from the results that the re-parametrised version has explored deeper into the funnel and further out on the rim. This is exactly what we want as it provides a better characterisation of the distribution.

## Showcase

### Cool things being done in stan

* My work isn't necessarily cool, but lots of it is done in stan.
* The open-source project "prophet" out of facebook research is a prediction library built in stan.

#### Examples that might be relevant for the rest of the workshop

* PK-PD models in stan. There are many of these available online

#### Where to get help

* To get further information there are some pretty active google groups for stan users and there is a stan conference.
* There are many resources on the [stan website](http://mc-stan.org/users/documentation/index.html), including a link to the [manual](https://github.com/stan-dev/stan/releases/download/v2.16.0/stan-reference-2.16.0.pdf).

#### Available interfaces

* There is probably one for your favourite language and making the most of Rstan.

### Related projects

#### JAGS and BUGS

* A couple of historic projects which are none the less worth knowing about. They both use Gibb's sampling, hence the "g" in their names. This is less efficient than stan's HMC but they have a long history so there may be existing implementations for your problem.

#### Greta

* Nick Golding is extending these methods to big data. This is just an R library so there is no need to learn any new syntax.
