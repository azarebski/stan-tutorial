functions {
  real[] logistic_diff(real time, real[] s, real[] theta, real[] x_r, int[]x_i) {
    real dsdt[1];
    dsdt[1] = theta[1] * s[1] * (1 - (s[1] / theta[2]));
    return dsdt;
  }

  real[] logistic_curve(int num_ts, real[] ts, int num_obs, int[] obs_ts, real s_0, real r, real K, real[] x_r, int[] x_i) {
    real sol[num_ts,1];
    real s[num_obs];
    real y0[1];
    real theta[2];
    y0[1] = s_0;
    theta[1] = r;
    theta[2] = K;
    sol = integrate_ode_rk45(logistic_diff, y0, 0, ts, theta, x_r, x_i);
    for (obs in 1:num_obs) {
      s[obs] = sol[obs_ts[obs],1];
    }
    return s;
  }
}


data {
  int num_times;
  real times[num_times];
  int num_obs;
  int Y[num_obs];
  int obs_times[num_obs];
}

transformed data {
  real x_r[0];
  int x_i[0];
}

parameters {
  real<lower=0> s_0;
  real<lower=0> K;
  real<lower=0> r;
}

transformed parameters {
  real s[num_obs];
  s = logistic_curve(num_times, times, num_obs, obs_times, s_0, r, K, x_r, x_i);
}

model {
  s_0 ~ normal(1000, 200);
  K ~ lognormal(log(1000), 1);
  r ~ rayleigh(0.1);
  for (t in 1:num_obs) {
    Y[t] ~ poisson(s[t]);
  }
}

generated quantities {
  real seal_pop[num_times];
  {
    int times_int[num_times];
    for (ix in 1:num_times) {
      times_int[ix] = ix;
    }
    seal_pop = logistic_curve(num_times, times, num_times, times_int, s_0, r, K, x_r, x_i);
  }
}
