library(rstan)
library(dplyr)
library(reshape2)


df <- read.csv("seals.csv")
df$times <- df$year - min(df$year) + 1
obs_mask <- !is.na(df$average_annual_count)
obs_df <- df[obs_mask, ]

num_times <- nrow(df)
times <- df$times
num_obs <- nrow(obs_df)
Y <- obs_df$average_annual_count
obs_times <- obs_df$times

post_samples <- stan("logistic.stan")

ggsave("diagnostic_ess.png", stan_ess(post_samples))
ggsave("diagnostic_rhat.png", stan_rhat(post_samples))
ggsave("diagnostic_trace.png", stan_trace(post_samples, "lp__"))

post_df <- as.data.frame(post_samples)

xx <- seq(1000, 1300, length.out = 100)
yy <- dnorm(xx, mean = 1000, sd = 200) / pnorm(0, mean = 1000, sd = 200, lower.tail = F)
g_ic <- ggplot() +
  geom_histogram(data = post_df, mapping = aes(x = s_0, y = ..density..)) +
  geom_line(data = data.frame(x = xx, y = yy), mapping = aes(x = x, y = y)) +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line())
  labs(x = "Initial condition", y = "")
ggsave("initial_condition.png", g_ic)

xx <- seq(7300, 7800, length.out = 100)
yy <- dlnorm(xx, mean = log(1E3), sd = 1)
g_carry <- ggplot() +
  geom_histogram(data = data.frame(x = post_df$K), mapping = aes(x = x, y = ..density..)) +
  geom_line(data = data.frame(x = xx, y = yy), mapping = aes(x = x, y = y)) +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line())
  labs(x = "Carrying capacity", y = "")
ggsave("carrying_capacity.png", g_carry)

drayleigh <- function(x, s) {
  x * exp(- (x ^ 2) / (2 * (s ^ 2))) / (s ^ 2)
}
xx <- seq(0.2, 0.3, length.out = 100)
yy <- drayleigh(xx, 0.2)
g_ray <- ggplot() +
  geom_histogram(data = data.frame(x = post_df$r), mapping = aes(x = x, y = ..density..)) +
  geom_line(data = data.frame(x = xx, y = yy), mapping = aes(x = x, y = y)) +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line()) +
  labs(x = "Growth rate", y = "")
ggsave("growth_rate.png", g_ray)


seals <- select(post_df, starts_with("seal_pop"))
names(seals) <- as.character(1:25)
seals <- melt(seals, id.vars = c()) %>%
  group_by(variable) %>%
  summarise(m = mean(value),
    ymax = quantile(value, 0.975),
    ymin = quantile(value, 0.025),
    obs_max = mean(qpois(0.975, lambda = value)),
    obs_min = mean(qpois(0.025, lambda = value))) %>%
  mutate(time = as.integer(as.character(variable)))
g_fit <- ggplot() +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = obs_min, ymax = obs_max), fill = "green", alpha = 0.4) +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "blue", alpha = 0.4) +
  geom_line(data = seals, mapping = aes(x = time, y = m), colour = "blue") +
  geom_point(data = data.frame(x = obs_times, y = Y), mapping = aes(x = x, y = y), colour = "red") +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line()) +
  labs(x = "Year", y = "Observed population")
ggsave("model_fit.png", g_fit)


post_samples <- stan("logisticNB.stan")

post_df <- as.data.frame(post_samples)

seals <- select(post_df, starts_with("seal_pop"))
names(seals) <- as.character(1:25)
seals <- melt(seals, id.vars = c()) %>%
  group_by(variable) %>%
  summarise(m = mean(value),
    ymax = quantile(value, 0.975),
    ymin = quantile(value, 0.025)) %>%
  mutate(time = as.integer(as.character(variable)))

temp <- select(post_df, starts_with("seal"), phi)
names(temp) <- c(as.character(1:25), "phi")
seals_green <- melt(temp, id.vars = c("phi")) %>%
  group_by(variable) %>%
  summarise(ymax = mean(qnbinom(0.975, mu = value, size = phi)),
    ymin = mean(qnbinom(0.025, mu = value, size = phi))) %>%
  mutate(time = as.integer(as.character(variable)))

g_fit <- ggplot() +
  geom_ribbon(data = seals_green, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "green", alpha = 0.4) +
  geom_ribbon(data = seals, mapping = aes(x = time, ymin = ymin, ymax = ymax), fill = "blue", alpha = 0.4) +
  geom_line(data = seals, mapping = aes(x = time, y = m), colour = "blue") +
  geom_point(data = data.frame(x = obs_times, y = Y), mapping = aes(x = x, y = y), colour = "red") +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line()) +
  labs(x = "Year", y = "Observed population")
ggsave("model_fit_nb.png", g_fit)

xx <- seq(7300, 7800, length.out = 100)
yy <- dlnorm(xx, mean = log(1E3), sd = 1)
g_carry <- ggplot() +
  geom_histogram(data = data.frame(x = post_df$K), mapping = aes(x = x, y = ..density..)) +
  geom_line(data = data.frame(x = xx, y = yy), mapping = aes(x = x, y = y)) +
  theme_classic() +
  theme(axis.line.x = element_line(), axis.line.y = element_line())
  labs(x = "Carrying capacity", y = "")
ggsave("carrying_capacity_nb.png", g_carry)
