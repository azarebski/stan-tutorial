library(rstan)
library(ggplot2)

fit_a <- stan("funnel_a.stan")
df_a <- as.data.frame(fit_a)
df_a$model <- "a"

fit_b <- stan("funnel_b.stan")
df_b <- as.data.frame(fit_b)
df_b$model <- "b"

cols <- c("y", "x[1]", "model")
df <- rbind(df_a[,cols], df_b[,cols])
names(df) <- c("y", "x", "z")
write.table(df, file = "neal_funnel.csv", quote = F, sep = ",", row.names = F)

g <- ggplot(df, aes(x = x, y = y)) +
  geom_point() +
  facet_wrap(~z) +
  theme_classic()
ggsave("neal_funnel.png", g, width = 15, height = 7)
