data {
  int num_obs;
  matrix[num_obs,3] X;
  real Y[num_obs];
}

parameters {
  vector[3] beta;
}

model {
  beta[1] ~ normal(0, 2);
  beta[2] ~ normal(0, 2);
  beta[3] ~ normal(0, 2);
  for (i in 1:num_obs) {
    Y[i] ~ normal(X[i, 1] * beta[1] + X[i, 2] * beta[2] + X[i, 3] * beta[3], 1);
  }
}
