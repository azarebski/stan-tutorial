data {
  int num_obs;
  matrix[num_obs,3] X;
  real Y[num_obs];
}

parameters {
  vector[3] beta;
}

model {
  beta ~ normal(0, 2);
  Y ~ normal(X * beta, 1);
}
