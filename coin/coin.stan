data {
  int n;
  int x;
}

parameters {
  real<lower=0,upper=1> p;
}

model {
  p ~ beta(1,1);
  x ~ binomial(n, p);
}
